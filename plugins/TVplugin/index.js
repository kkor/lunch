angular.module('tv.tvplugin', [])
.config(['c8yViewsProvider', function (c8yViewsProvider) {
    c8yViewsProvider.when('/device/:deviceId', {
        name: 'TeamViewer',
        icon: 'envelope-o',
        priority: 1001,
        templateUrl: ':::PLUGIN_PATH:::/views/TeamViewerDevice.html',
        controller: 'TVDeviceCtrl'
    });
}]);