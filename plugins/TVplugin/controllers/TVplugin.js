angular.module('tv.tvplugin')
.controller('TVDeviceCtrl', ['$scope', '$routeParams', 'c8yDevices', 'c8yAlert',
    function ($scope, $routeParams, c8yDevices, c8yAlert) {
        'use strict';
          function load() {
            c8yDevices.detail($routeParams.deviceId).then(function (res) {
                var device = res.data;
                $scope.device.id = device.id;
                $scope.device.c8y_TeamViewer = device.c8y_TeamViewer;
            });
        }

        $scope.device = {};

        load();
    }
]);