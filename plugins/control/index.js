angular.module('tv.tvConnectControl', [])
.config(['c8yViewsProvider', function (c8yViewsProvider) {
    c8yViewsProvider.when('/device/:deviceId', {
        name: 'Control-BUT',
        icon: 'envelope-o',
        priority: 1001,
        templateUrl: ':::PLUGIN_PATH:::/views/index.html',
        controller: 'deviceDetailControlCtrl'
    });
}]);