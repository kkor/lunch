/* core 6.17.8 2016-03-17T18:11:22+01:00 1c10f76c08e2+ (hotfix/6.17.8) tip */
angular.module("tv.tvConnectControl", []).config(["c8yViewsProvider", "gettext", function(e, n) {
        "use strict";
        var a = "/apps/lunch/control/views/",
            t = "/device/:deviceId";
        e.when(t, {
            templateUrl: a + "index.html",
            priority: 600,
            name: n("Control-But"),
            icon: "gamepad",
            controller: ["$scope", "$routeParams", "c8yDevices", function(e, n, a) {
                e.device = {}, a.detail(n.deviceId).then(function(n) {
                    e.device = n.data
                })
            }]
        })
    }]), angular.module("tv.tvConnectControl").controller("deviceDetailControlCtrl", ["$scope", "c8yBase", "c8yDevices", "c8yDeviceControl", "c8yAlert", "gettext", "gettextCatalog", function(e, n, a, t, i, r, s) {
        "use strict";

        function l(n) {
            var a = !1;
            if (e.device) {
                var t = c("c8y_TraceStatus"),
                    i = c("c8y_Restart"),
                    r = c("c8y_Relay"),
                    s = c("c8y_Message"),
                    l = c("c8y_RelayArray");
                a = "trace" === n ? t : "restart" === n ? i : "relay" === n ? r : "message" === n ? s : "relayarray" === n ? l : t || i || r || s || l
            }
            return a
        }

        function c(n) {
            return a.supportsOperation(e.device, n)
        }

        function d() {
            v("ON")
        }

        function o() {
            v("OFF")
        }

        function v(e) {
            f({
                description: s.getString("Trace status to {{traceState}}", {
                    traceState: e
                }),
                c8y_TraceStatus: {
                    state: e
                }
            })
        }

        function y() {
            f({
                description: r("Restart device"),
                c8y_Restart: {}
            })
        }

        function p(n) {
            e.device.c8y_Relay && e.device.c8y_Relay.relayState && (e.device.c8y_Relay.relayState = n), f({
                description: s.getString("Update relay status to {{relayStatus}}", {
                    relayStatus: n
                }),
                c8y_Relay: {
                    relayState: n
                }
            })
        }

        function u(n, a) {
            var t = {};
            t.description = ("CLOSED" === n ? "Closing" : "Opening") + " relay " + (a + 1) + ".", t.c8y_RelayArray = e.device.c8y_RelayArray, t.c8y_RelayArray[a] = n, f(t)
        }

        function g(e) {
            f({
                description: s.getString('Send message "{{msg}}"', {
                    msg: e
                }),
                c8y_Message: {
                    text: e
                }
            })
        }

        function f(n, a) {
            return n.deviceId = e.device.id, t.create(n).then(function() {
                b(n, a)
            })
        }

        function b(n, a) {
            i.success(s.getString('Created operation: "{{opDescription}}"!', {
                opDescription: n.description
            })), a && e.$emit("update", "devicecontrol")
        }
        e.visible = l, e.supportsOperation = c, e.traceStatusOn = d, e.traceStatusOff = o, e.restart = y, e.relay = p, e.relayarray = u, e.message = g
    }]),
    function() {
        "use strict";

        function e(e) {
            e.put("/apps/lunch/control/views/index.html", '<div><p>hackor</p></div><div class="row" ng-controller="deviceDetailControlCtrl">\n  <div class="col-lg-4" ng-show="visible()">\n    <div ng-show="visible(\'trace\')">\n      <div class="panel panel-default">\n        <div class="panel-heading">\n          <i c8y-icon="bug"></i>\n          <translate>Trace status</translate>\n        </div>\n        <div class="panel-body">\n          <p translate>Send an operation to start or stop device internal tracing. Result of tracing is device dependent, it can generate more events or generate a file inside the device.</p>\n        </div>\n        <div class="panel-footer">\n          <div class="btn-group">\n            <a class="btn btn-primary" href="" ng-click="traceStatusOn(deviceDetail)" translate>On</a>\n            <a class="btn btn-default" href="" ng-click="traceStatusOff(deviceDetail)" translate>Off</a>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div ng-show="visible(\'restart\')">\n      <div class="panel panel-default">\n        <div class="panel-heading">\n          <i c8y-icon="refresh"></i>\n          <translate>Restart device</translate>\n        </div>\n        <div class="panel-body">\n          <p translate>Send an operation to restart the device. The process of restart can be monitored under the operations of the device.</p>\n        </div>\n        <div class="panel-footer">\n          <div class="controls">\n            <button class="btn btn-primary" ng-click="restart(deviceDetail)" translate>Restart</button>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div ng-show="visible(\'relay\')">\n      <div class="panel panel-default">\n        <div class="panel-heading">\n          <i c8y-icon="lightbulb-o"></i>\n          <translate>Relay</translate>\n        </div>\n        <div class="panel-body">\n          <p translate>Send an operation to switch a relay on or off.</p>\n        </div>\n        <div class="panel-footer">\n          <div class="btn-group">\n            <a class="btn btn-{{device.c8y_Relay.relayState===\'CLOSED\'?\'primary\':\'default\'}}" href="" ng-click="relay(\'CLOSED\')" translate>On</a>\n            <a class="btn btn-{{device.c8y_Relay.relayState===\'OPEN\'?\'primary\':\'default\'}}" href="" ng-click="relay(\'OPEN\')" translate>Off</a>\n          </div>\n        </div>\n      </div>\n    </div>\n	\n	<div ng-show="visible(\'relayarray\')">\n      <div class="panel panel-default">\n        <div class="panel-heading">\n          <i c8y-icon="lightbulb-o"></i>\n          <translate>Array of Relays</translate>\n        </div>\n        <div class="panel-body">\n          <p translate>Send an operation to switch an array of relays on or off.</p>\n        </div>\n        <div class="panel-footer">\n          <div class="btn-group">\n            <a ng-repeat="rl in device.c8y_RelayArray track by $index" class="btn btn-{{rl===\'CLOSED\'?\'primary\':\'default\'}}" ng-click="relayarray(rl===\'CLOSED\'?\'OPEN\':\'CLOSED\', $index)">{{$index + 1}}</a>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div ng-show="visible(\'message\')">\n      <div class="panel panel-default">\n        <div class="panel-heading">\n          <i c8y-icon="comment"></i>\n          <translate>Send message</translate>\n        </div>\n        <div class="panel-body">\n          <p translate>Send an operation to show a particular message to the device. The device behavior is device dependent.</p>\n          <div class="form-group">\n            <input type="text" class="form-control" ng-model="str">\n          </div>\n        </div>\n        <div class="panel-footer">\n          <button class="btn btn-primary" ng-click="message(str)" ng-disabled="!str" translate>Send</button>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div ng-class="{\'col-lg-12\': !visible(), \'col-lg-8\': visible()}" ng-include="\'/apps/lunch/control/views/index.html\'"></div>\n</div>')
        }
       // angular.module("tv.tvConnectControl").run(["$templateCache", e])
    }();